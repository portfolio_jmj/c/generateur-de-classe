﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;


namespace AutoMapper
{
    class Program
    {
        //protected string connectionString { get; set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            GetConnection();
        }

        static void GetConnection()
        {
            Console.WriteLine("Veuillez saisir : ");
            Console.Write("1/3 - Ip de la base de donnée SQL SERVER : ");
            Console.CursorVisible = true;
            string dataSource = Console.ReadLine();
            Console.Write("2/3 - le nom d'utilisateur : ");
            string userID = Console.ReadLine();
            Console.Write("3/3 - le mot de passe : ");
            string password = Console.ReadLine();

            Console.CursorVisible = false;

            string alterCoString = "Data Source=" + dataSource + ";Persist Security Info=True;User ID=" + userID + ";Password=" + password;
            Console.WriteLine(alterCoString);

            //string connectionString = "Data Source=192.168.0.15;Persist Security Info=True;User ID=webuse;Password=webuse";
            SqlConnection connection = new SqlConnection(alterCoString);
            ShowDb(connection);

        }

        static void ShowDb(SqlConnection connection)
        {
            Boolean continious = true;
            while (continious)
            {
                try
                {
                    string query = "SELECT name FROM master.sys.databases WHERE name NOT IN('master', 'tempdb', 'model', 'msdb') ORDER BY name ASC;";

                    SqlCommand command = new SqlCommand(query, connection);
                    command.Connection.Open();


                    SqlDataReader reader = command.ExecuteReader();

                    //UI
                    Console.WriteLine("Voici la liste des bases de données accessible depuis le serveur");

                    int i = 0;
                    List<string> databases = new List<string>();
                    // Call Read before accessing data.
                    while (reader.Read())
                    {
                        IDataRecord record = (IDataRecord)reader;
                        Console.WriteLine(String.Format(" {0} : {1}", i, record[0]));
                        databases.Add(record[0].ToString());
                        ++i;
                    }
                    // Call Close when done reading.
                    reader.Close();
                    //connection.Close();

                    Console.Write("Selectionner la table  : ");
                    Console.CursorVisible = true;
                    string database = Console.ReadLine();
                    Console.CursorVisible = false;

                    if (database.Length <= 2)
                        database = databases[Int16.Parse(database)];

                    string queryTable = "USE " + database + "; USE EXTERNAL_DATA; SELECT TABLE_NAME, TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES ORDER BY TABLE_NAME ASC;";
                    SqlCommand command2 = new SqlCommand(queryTable, connection);
                    //command.Connection.Open();

                    SqlDataReader readerTable = command2.ExecuteReader();

                    //UI
                    Console.WriteLine("Voici la liste des tables pour la base de données {0}", database);

                    int j = 0;
                    List<string> tables = new List<string>();
                    // Call Read before accessing data.
                    while (readerTable.Read())
                    {
                        IDataRecord recordTable = (IDataRecord)readerTable;
                        Console.WriteLine(String.Format(" {0}   : {1} : {2}", j, recordTable[0], recordTable[1])); ;
                        tables.Add(recordTable[0].ToString());
                        ++j;
                    }
                    // Call Close when done reading.
                    readerTable.Close();

                    Console.Write("Selectionner la table : ");
                    Console.CursorVisible = true;
                    string table = Console.ReadLine();
                    Console.CursorVisible = false;

                    if (table.Length <= 2)
                        table = tables[Int16.Parse(table)];

                    string queryColumn = "USE " + database + "; SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + table + "' ;";
                    SqlCommand commandColumn = new SqlCommand(queryColumn, connection);

                    SqlDataReader readerColumn = commandColumn.ExecuteReader();

                    List<Column> columns = new List<Column>();
                    while (readerColumn.Read())
                    {
                        IDataRecord recordColumn = (IDataRecord)readerColumn;

                        Column c = new Column();
                        c.columnName = (readerColumn[0].ToString() != "") ? readerColumn[0].ToString() : "";
                        c.dataType = (readerColumn[1].ToString() != "") ? readerColumn[1].ToString() : "";
                        c.characterMaximumLength = (readerColumn[2].ToString() != "") ? readerColumn[2].ToString() : "";
                        c.numericPrecision = (readerColumn[3].ToString() != "") ? readerColumn[3].ToString() : "";
                        c.numericScale = (readerColumn[4].ToString() != "") ? readerColumn[4].ToString() : "";
                        c.isNullable = readerColumn[5].ToString();
                        columns.Add(c);
                    }

                    #region File
                    string s = System.IO.Directory.GetCurrentDirectory();
                    Console.WriteLine(s);


                    Console.Write("Ecrire le nom de la classe avec une majuscule au début: ");
                    Console.CursorVisible = true;
                    string fileName = Console.ReadLine().Trim();
                    Console.CursorVisible = false;
                    string path = @".\" + fileName.ToLower() + ".cs";

                    using (System.IO.FileStream fs = System.IO.File.Create(path))
                    {
                        Byte[] start = new UTF8Encoding(true).GetBytes("using System;  \r\n\r\n namespace ChangeMe \r\n{\r\n\tclass " + fileName + "\r\n\t{\r\n");
                        fs.Write(start, 0, start.Length);

                        //devra lancer un module dédié à l'environnement de bdd içi il n'y a que SQL SERVER
                        //devra être dans la fonction de construction de ligne
                        ConvertType cv = new ConvertType();
                        columns.ForEach(delegate (Column col)
                        {

                            //createAttribut(col, fs, cv);
                            createAttributBudget(col, fs, cv);

                        });

                        Byte[] end = new UTF8Encoding(true).GetBytes("\t}\r\n}");
                        fs.Write(end, 0, end.Length);

                    }

                    Console.Write("Souhaitez vous générer une nouvelle classe ? : (oui/non)");
                    string u = Console.ReadLine();
                    continious =  (u == "oui") ? true : false;

                    connection.Close();


                    #endregion

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }

        static void createAttribut(Column column, System.IO.FileStream fs, ConvertType cv)
        {
            string tab = "\t\t";
            string type;
            cv.dbMap.TryGetValue(column.dataType, out type);
            string getSet = @" { set; get; }";
            Byte[] line = new UTF8Encoding(true).GetBytes(tab + "public " + type + ' ' + column.columnName + getSet + "\r\n");
            fs.Write(line,0,line.Length);
            return;

        }

        static void createAttributBudget(Column column, System.IO.FileStream fs, ConvertType cv)
        {
            string tab = "\t\t";
            string type;
            cv.dbMap.TryGetValue(column.dataType, out type);
            Byte[] priv = new UTF8Encoding(true).GetBytes(tab + "private " + type + " _" + column.columnName + ";\r\n");
            fs.Write(priv, 0, priv.Length);
            string getSet = @" { set; get; }";
            Byte[] pub = new UTF8Encoding(true).GetBytes(tab + "public " + type + " " + column.columnName + "\r\n\t\t{ \r\n\t" + tab + "get { return _" + column.columnName + "; }\r\n\t" + tab + "set { _" + column.columnName + " = value; }\r\n" + tab + "}\r\n\r\n");
            fs.Write(pub, 0, pub.Length);
            return;
        }


        static void createMethods(System.IO.FileStream fs)
        {
            //getAll
            //get
            //update
            //delete
            //
        }

    }

    class Column
    {
        public String columnName { get; set; }
        public string dataType { get; set; }
        public string characterMaximumLength { get; set; }
        public string numericPrecision { get; set; }
        public string numericScale { get; set; }
        public string isNullable { get; set; }
        public Boolean toto { get; set; }
    }

    class ConvertType
    {
        public Dictionary<String, String> dbMap = new Dictionary<String, String>();

        public ConvertType()
        {
            this.dbMap.Add("bigint", "Int64");
            this.dbMap.Add("binary", "Byte[]");
            this.dbMap.Add("bit", "Boolean");
            this.dbMap.Add("char", "String");
            this.dbMap.Add("date", "DateTime");
            this.dbMap.Add("datetime", "DateTime");
            this.dbMap.Add("datetime2", "DateTime");
            this.dbMap.Add("datetimeoffset", "DateTimeOffset");
            this.dbMap.Add("decimal", "Decimal");
            this.dbMap.Add("float", "Double");
            this.dbMap.Add("image", "Byte[]");
            this.dbMap.Add("int", "Int32");
            this.dbMap.Add("money", "Decimal");
            this.dbMap.Add("nchar", "String");
            this.dbMap.Add("ntext", "String");
            this.dbMap.Add("numeric", "Decimal");
            this.dbMap.Add("nvarchar", "String");
            this.dbMap.Add("real", "Single");
            this.dbMap.Add("rowversion", "Byte[]");
            this.dbMap.Add("smalldatetime", "DateTime");
            this.dbMap.Add("smallint", "Int16");
            this.dbMap.Add("smallmoney", "Decimal");
            this.dbMap.Add("text", "String");
            this.dbMap.Add("Char[]", "Text");
            this.dbMap.Add("time", "TimeSpan");
            this.dbMap.Add("timestamp", "Byte[]");
            this.dbMap.Add("tinyint", "Byte");
            this.dbMap.Add("uniqueidentifier", "Guid");
            this.dbMap.Add("varbinary", "Byte[]");
            this.dbMap.Add("varchar", "String");
            this.dbMap.Add("xml", "Xml");
        }
        
       
    }

}
